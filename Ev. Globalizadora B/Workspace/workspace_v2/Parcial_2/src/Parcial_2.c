#include "chip.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "string.h"
#include "stdio.h"


//-------------------------------- Variables Globales ------------------------------

static ADC_CLOCK_SETUP_T ADCSetup; //Se setea la Configuración del ADC: rate,accuracy, mode const, por Default la función setea el ADC en 200kHz.
portTickType xTicksToWait = 10 / portTICK_RATE_MS; //Constante de tiempo para el manejo de cola
void UART3PutChar(char ch); //Prototipo de la función PutChar
void UART3PutSTR(char *str); //Prototipo de la función PutString
xSemaphoreHandle xSemaforo;  //Declaración del semáforo
xQueueHandle xColaString_0; //Declaración de la cola para el sensor 0
xQueueHandle xColaString_1; //Declaración de la cola para el sensor 1
xQueueHandle xColaString_2; //Declaración de la cola para el sensor 2
xQueueHandle xColaString_3; //Declaración de la cola para el sensor 3
char *itoa(int x,char *g,int y);

//------------------- Tarea para obtener el valor de los sensores con el ADC ---------------------

static void Tarea_ADC(void *pvParameters) {

  Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE); //Se Habilita el ADC para leer en modo ráfaga

  // variables locales de la tarea para calculo y escritura
  uint8_t x=0,n0=0, n1=0, n2=0, n3=0;
  uint16_t dataADC;
  uint32_t temp_0=0, temp_1=0, temp_2=0, temp_3=0;
  uint16_t dataADCprom0=0;
  uint16_t dataADCprom1=0;
  uint16_t dataADCprom2=0;
  uint16_t dataADCprom3=0;

  char vstring_0 [3];
  char vstring_1 [3];
  char vstring_2 [3];
  char vstring_3 [3];
  float kelvin_0=0, kelvin_1=0, kelvin_2=0, kelvin_3=0;
  float Centigrado_0=0, Centigrado_1=0, Centigrado_2=0, Centigrado_3=0;

  // Bucle de operación
  while(1) {
    xSemaphoreTake (xSemaforo, portMAX_DELAY); // Toma el semáforo
    uint32_t dataADCaux0=0, dataADCaux1=0, dataADCaux2=0, dataADCaux3=0;
    int q; // Tomo 200 muestras de valores del ADC
    for(q=0;q<200;q++) {
        while (Chip_ADC_ReadStatus(LPC_ADC, ADC_CH0, ADC_DR_DONE_STAT) != SET) {} // Se indica que se lee el channel 0
        Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &dataADC); // Se toma el valor del canal 0
        dataADCaux0 = dataADCaux0+dataADC; // Se guardan los valores de las muestras de temp
        Chip_ADC_ReadValue(LPC_ADC, ADC_CH1, &dataADC); // Se toma el valor del canal 1
        dataADCaux1 = dataADCaux1+dataADC; // Se guardan los valores de las muestras de temp
        Chip_ADC_ReadValue(LPC_ADC, ADC_CH2, &dataADC); // Se toma el valor del canal 2
        dataADCaux2 = dataADCaux2+dataADC; // Se guardan los valores de las muestras de temp
        Chip_ADC_ReadValue(LPC_ADC, ADC_CH3, &dataADC); // Se toma el valor del canal 3
        dataADCaux3 = dataADCaux3+dataADC; // Se guardan los valores de las muestras de temp

        vTaskDelay(10); // Se muestrea cada 100 ms

    }
    // Para obtener una medida estable, se promedian las 200 mediciones
    dataADCprom0 = dataADCaux0/200; // Cálculo del promedio de las muestras de CH0
    dataADCprom1 = dataADCaux1/200; // Cálculo del promedio de las muestras de CH1
    dataADCprom2 = dataADCaux2/200; // Cálculo del promedio de las muestras de CH2
    dataADCprom3 = dataADCaux3/200; // Cálculo del promedio de las muestras de CH3

    // Pasamos el valor de ADC, a un valor de Temperatura en grados Kelvin/100. Son los 3,3V de alimentacion sobre 4096 que son los bits de resolucion del canal
    kelvin_0 = dataADCprom0*3.3/4096; // Constante de tensión / rango de 12 bits del canal
    kelvin_1 = dataADCprom1*3.3/4096; // Constante de tensión / rango de 12 bits del canal
    kelvin_2 = dataADCprom2*3.3/4096; // Constante de tensión / rango de 12 bits del canal
    kelvin_3 = dataADCprom3*3.3/4096; // Constante de tensión / rango de 12 bits del canal
    
    // Chequeamos si está apretado el pulsados, con el cual se cambia la resolución de la medicion (Feature pedido el lunes)
    _Bool prec = false;
    Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
    vTaskDelay (200); //Aguardo 2 segundos entre cada medición
    Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1);
    _Bool boton=Chip_GPIO_GetPinState(LPC_GPIO,0,7); // Pin 7, p0.7
    // Si está apretado, mido con mayor presicion
    if (boton == false)
      prec = true;

    // Pasamos de grados la medicion a grados sentigrados
    Centigrado_0 = kelvin_0 - 2.73; // Se pasa de grado Kelvin a Centígrado la medicion del sensor 0
    temp_0 = Centigrado_0*100; // Constante de 10 mv/grado, Dada por el LM335
    if(prec == false) {     // Si está seteado en baja presicion. Se ignoran los últimos 3 bits de la variable de temperatura, aplicando una máscara con 120 (1111000)
      temp_0=temp_0 & 120;
    }
    itoa(temp_0,vstring_0,10); // Se convierte a string la medicion del sensor 0
    n0=strlen(vstring_0);

    Centigrado_1 = kelvin_1 - 2.73; // Se pasa de grado Kelvin a Centígrado la medicion del sensor 1
    temp_1 = Centigrado_1*100; // Constante de 10 mv/grado, Dada por el LM335
    if(prec == false) {    // Si está seteado en baja presicion. Se ignoran los últimos 3 bits de la variable de temperatura, aplicando una máscara con 120 (1111000)
      temp_1=temp_1 & 120;
    }
    itoa(temp_1,vstring_1,10); // Se convierte a string la medicion del sensor 1
    n1=strlen(vstring_1);

    Centigrado_2 = kelvin_2 - 2.73; // Se pasa de grado Kelvin a Centígrado la medicion del sensor 2
    temp_2 = Centigrado_2*100; // Constante de 10 mv/grado, Dada por el LM335
    if(prec == false) {     // Si está seteado en baja presicion. Se ignoran los últimos 3 bits de la variable de temperatura, aplicando una máscara con 120 (1111000)
      temp_2=temp_2 & 120;
    }
    itoa(temp_2,vstring_2,10); // Se convierte a string la medicion del sensor 2
    n2=strlen(vstring_2);

    Centigrado_3 = kelvin_3 - 2.73; // Se pasa de grado Kelvin a Centígrado la medicion del sensor 3
    temp_3 = Centigrado_3*100; // Constante de 10 mv/grado, Dada por el LM335
    if(prec == false) {     // Si está seteado en baja presicion. Se ignoran los últimos 3 bits de la variable de temperatura, aplicando una máscara con 120 (1111000) 
      temp_3=temp_3 & 120;
    }
    itoa(temp_3,vstring_3,10); // Se convierte a string la medicion del sensor 3
    n3=strlen(vstring_3);

    for(x=n0;x<3;x++){
    // Se almacena el valor de temperatura en el string del sensor 0
      vstring_0[x]=' ';
    }

    for(x=n1;x<3;x++){
    // Se almacena el valor de temperatura en el string del sensor 1
      vstring_1[x]=' ';
    }

    for(x=n2;x<3;x++){
    // Se almacena el valor de temperatura en el string del sensor 2
      vstring_2[x]=' ';
    }
    for(x=n3;x<3;x++){
    // Se almacena el valor de temperatura en el string del sensor 3
      vstring_3[x]=' ';
    }

    // Poniendo como primer caracter valor del string, para que la funcion sepa cuando termina.
    vstring_0[3]=0;
    vstring_1[3]=0;
    vstring_2[3]=0;
    vstring_3[3]=0;

    // Poniendo el string con la temperatura de cada sensor en la cola de cada sensor
    xQueueSend(xColaString_0,&vstring_0,xTicksToWait);// Se envía el valor a la cola del sensor 0
    xQueueSend(xColaString_1,&vstring_1,xTicksToWait);// Se envía el valor a la cola del sensor 1
    xQueueSend(xColaString_2,&vstring_2,xTicksToWait);// Se envía el valor a la cola del sensor 2
    xQueueSend(xColaString_3,&vstring_3,xTicksToWait);// Se envía el valor a la cola del sensor 3

    xSemaphoreGive(xSemaforo); // Se libera el semáforo
  }
}

//------------------- Tarea para comunicación por RS232 a PC ---------------------

static void taskRS232 (void) {
  char vstring_0 [3], vstring_1 [3], vstring_2 [3], vstring_3 [3]; //Vector para recibir el valor almacenado en cola
  char value_0[]="Temp1: "; //Texto para lectura de sensor 0
  char value_1[]="Temp2: "; //Texto para lectura de sensor 1
  char value_2[]="Temp3: "; //Texto para lectura de sensor 2
  char value_3[]="Temp4: "; //Texto para lectura de sensor 3
  char mostrar[]={0x0d,0x0a,0}; //Salto de línea de retorno de carro

  while(1) {
    // Seccion para tomar semaforos y datos de los queues
    xSemaphoreTake (xSemaforo, portMAX_DELAY); // Se Toma de semáforo
    xQueueReceive(xColaString_0,&vstring_0,xTicksToWait); // Se Toma del valor de la cola del sensor 0
    xQueueReceive(xColaString_1,&vstring_1,xTicksToWait); // Se Toma del valor de la cola del sensor 1
    xQueueReceive(xColaString_2,&vstring_2,xTicksToWait); // Se Toma del valor de la cola del sensor 2
    xQueueReceive(xColaString_3,&vstring_3,xTicksToWait); // Se Toma del valor de la cola del sensor 3

    // Seccion de envio de datos por UART
    UART3PutSTR(value_0); // Se Envía de texto de presentación del sensor 0
    vTaskDelay(100);
    UART3PutSTR(vstring_0); //Se Envía el valor de temperatura del sensor 0
    vTaskDelay(100);
    UART3PutSTR(mostrar); //Se envía el salto y retorno de carro
    vTaskDelay(100);
    UART3PutSTR(value_1); // Se Envía de texto de presentación del sensor 1
    vTaskDelay(100);
    UART3PutSTR(vstring_1); //Se Envía el valor de temperatura del sensor 1
    vTaskDelay(100);
    UART3PutSTR(mostrar); //Se envía el salto y retorno de carro
    vTaskDelay(100); //Espera para la lectura de valores
    UART3PutSTR(value_2); // Se Envía de texto de presentación del sensor 2
    vTaskDelay(100);
    UART3PutSTR(vstring_2); //Se Envía el valor de temperatura del sensor 2
    vTaskDelay(100);
    UART3PutSTR(mostrar); //Se envía el salto y retorno de carro
    vTaskDelay(100); //Espera para la lectura de valores
    UART3PutSTR(value_3); // Se Envía de texto de presentación del sensor 3
    vTaskDelay(100);
    UART3PutSTR(vstring_3); //Se Envía el valor de temperatura del sensor 3
    vTaskDelay(100);
    UART3PutSTR(mostrar); //Se envía el salto y retorno de carro del sensor 
    vTaskDelay(100); //Espera para la lectura de valores

    vTaskDelay(1000);
    xSemaphoreGive(xSemaforo); //Se libera el semáforo
  }
}

//----------------------- Función para inicializaciones ------------------------------

static void prvSetupHardware(void) {
  SystemCoreClockUpdate();
  Board_Init();
  Chip_ADC_Init(LPC_ADC, &ADCSetup);
  Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE); // Se habilita el canal 0: pin 15 p0.23
  Chip_ADC_EnableChannel(LPC_ADC, ADC_CH1, ENABLE); // Se habilita el canal 1: pin 16 p0.24
  Chip_ADC_EnableChannel(LPC_ADC, ADC_CH2, ENABLE); // Se habilita el canal 2: pin 17 p0.25
  Chip_ADC_EnableChannel(LPC_ADC, ADC_CH3, ENABLE); // Se habilita el canal 3: pin 18 p0.26
  Chip_IOCON_PinMux(LPC_IOCON,0,23,IOCON_MODE_INACT,IOCON_FUNC1);
  Chip_IOCON_PinMux(LPC_IOCON,0,24,IOCON_MODE_INACT,IOCON_FUNC1);
  Chip_IOCON_PinMux(LPC_IOCON,0,25,IOCON_MODE_INACT,IOCON_FUNC1);
  Chip_IOCON_PinMux(LPC_IOCON,0,26,IOCON_MODE_INACT,IOCON_FUNC1);
  Chip_UART_Init(LPC_UART3); // Se inicializa la UART3 p0.0 Tx, p0.1 Rx
  Chip_UART_SetBaud(LPC_UART3,9600); // Se setea la tasa de transmisión
  Chip_UART_ConfigData(LPC_UART3,UART_LCR_WLEN8|UART_LCR_SBS_1BIT| UART_LCR_PARITY_DIS); // Registro de control de línea para formato de cuadros y generación de rupturas.
  Chip_UART_SetupFIFOS(LPC_UART3, UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0); // UART FIFO nivel de disparo 0: 1 carácter
  Chip_UART_TXEnable(LPC_UART3); // Habilita la transmisión de datos
  Chip_GPIO_Init(LPC_GPIO); // Inicializa el modulo de GPIO
  Chip_GPIO_WriteDirBit(LPC_GPIO, 0, 7, false); //Pruebo con ésta segunda instrucción
}

//----------------------- MAIN ------------------------------
int main (void) {
  xTaskCreate(Tarea_ADC,"Tarea_ADC",1024,0,1,0); // Se crea la tarea para operar el ADC
  xTaskCreate(taskRS232,"taskRS232",1024,0,1,0); // Se crea la tarea para operar la UART
  xSemaforo = xSemaphoreCreateMutex(); // Se crea el semáforo
  xColaString_0 = xQueueCreate(1,sizeof(uint32_t)); // Se crea la cola para almacenar los valores de temperatura de sensor 0
  xColaString_1 = xQueueCreate(1,sizeof(uint32_t)); // Se crea la cola para almacenar los valores de temperatura de sensor 1
  xColaString_2 = xQueueCreate(1,sizeof(uint32_t)); // Se crea la cola para almacenar los valores de temperatura de sensor 2
  xColaString_3 = xQueueCreate(1,sizeof(uint32_t)); // Se crea la cola para almacenar los valores de temperatura de sensor 3
  prvSetupHardware(); // Se realizan la inicializaciones
  vTaskStartScheduler(); // Se instancia el Scheduler
}

//----------------------- Función para enviar un carácter por la UART ------------------------------

void UART3PutChar(char ch) {
  while ((Chip_UART_ReadLineStatus(LPC_UART3) & UART_LSR_THRE) == 0) {}
  Chip_UART_SendByte(LPC_UART3, (uint8_t) ch);
}

//----------------------- Función para enviar un string caracter por caracter a la UART ------------------------------
void UART3PutSTR(char *str) {
  while (*str != '\0') {
    UART3PutChar(*str++);
  }
}
