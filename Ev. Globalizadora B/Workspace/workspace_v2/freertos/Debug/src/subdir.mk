################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/FreeRTOSCommonHooks.c \
../src/cr_startup_lpc175x_6x.c \
../src/freertos_blinky.c \
../src/heap_3.c \
../src/list.c \
../src/port.c \
../src/queue.c \
../src/sysinit.c \
../src/tasks.c 

OBJS += \
./src/FreeRTOSCommonHooks.o \
./src/cr_startup_lpc175x_6x.o \
./src/freertos_blinky.o \
./src/heap_3.o \
./src/list.o \
./src/port.o \
./src/queue.o \
./src/sysinit.o \
./src/tasks.o 

C_DEPS += \
./src/FreeRTOSCommonHooks.d \
./src/cr_startup_lpc175x_6x.d \
./src/freertos_blinky.d \
./src/heap_3.d \
./src/list.d \
./src/port.d \
./src/queue.d \
./src/sysinit.d \
./src/tasks.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -D__REDLIB__ -DCORE_M3 -D__USE_LPCOPEN -D__LPC17XX__ -I"C:\Users\54112\Documents\MCUXpressoIDE_11.0.1_2563\workspace\freertos\inc" -I"C:\Users\54112\Documents\MCUXpressoIDE_11.0.1_2563\workspace\lpc_board_nxp_lpcxpresso_1769\inc" -I"C:\Users\54112\Documents\MCUXpressoIDE_11.0.1_2563\workspace\lpc_chip_175x_6x\inc" -I"C:\Users\54112\Documents\MCUXpressoIDE_11.0.1_2563\workspace\freertos\inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


